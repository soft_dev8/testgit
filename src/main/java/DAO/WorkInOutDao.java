/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Helper.DatabaseHelper;
import Model.WorkInOut;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author WIP
 */
public class WorkInOutDao implements Dao<WorkInOut> {

    @Override
    public WorkInOut get(int id) {
        WorkInOut item = null;
        String sql = "SELECT * FROM Work_In_Out WHERE WIO_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = WorkInOut.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<WorkInOut> getAll() {
        ArrayList<WorkInOut> list = new ArrayList();
        String sql = "SELECT * FROM Work_In_Out";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WorkInOut item = WorkInOut.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public WorkInOut save(WorkInOut obj) {
        String sql = "INSERT INTO Work_In_Out (WIO_ID, Date_Time_In, Date_Time_Out, Emp_ID) "
                + "VALUES (?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setDate(2, obj.getDateTimeIn());
            stmt.setDate(3, obj.getDateTimeOut());
            stmt.setInt(4, obj.getEmpID().getId());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertID(stmt);
            obj = get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public WorkInOut update(WorkInOut obj) {
        String sql = "UPDATE Work_In_Out"
                + " SET Date_Time_In = ?, Date_Time_Out = ?, Emp_ID = ?"
                + " WHERE WIO_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setDate(2, obj.getDateTimeIn());
            stmt.setDate(3, obj.getDateTimeOut());
            stmt.setInt(4, obj.getEmpID().getId());

            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(WorkInOut obj) {
        String sql = "DELETE FROM Work_In_Out WHERE WIO_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
