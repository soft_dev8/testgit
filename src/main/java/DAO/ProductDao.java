/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Helper.DatabaseHelper;
import Model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ACER
 */
public class ProductDao implements Dao<Product> {

    @Override
    public Product get(int id) {
        Product item = null;
        String sql = "SELECT * FROM Product WHERE Prod_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Product.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM Product";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product product = Product.fromRS(rs);
                list.add(product);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    @Override
    public Product save(Product obj) {
        String sql = "INSERT INTO Product (Prod_ID, Prod_Name , Prod_Price , Prod_Type)" + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setString(2, obj.getName());
            stmt.setDouble(3, obj.getPrice());
            stmt.setString(4, obj.getType());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertID(stmt);
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    @Override
    public Product update(Product obj) {
        String sql = "UPDATE Product" + " SET Prod_ID = ? Prod_Name = ?,Prod_Price = ?, Prod_Type = ?" + " WHERE Prod_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setString(2, obj.getName());
            stmt.setDouble(3, obj.getPrice());
            stmt.setString(4, obj.getType());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    @Override
    public int delete(Product obj) {
        ArrayList<Product> list = new ArrayList();
        String sql = "DELETE FROM Product";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product item = Product.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<Product> getAll(String where, String order) {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM Product  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product item = Product.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
