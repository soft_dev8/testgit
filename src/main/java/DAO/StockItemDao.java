/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Helper.DatabaseHelper;
import Model.StockItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HP
 */
public class StockItemDao implements Dao<StockItem>{
    
     public StockItem get(int id) {
        StockItem item = null;
        String sql = "SELECT * FROM Stock_Item WHERE Mat_Item_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = StockItem.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<StockItem> getAll() {
        ArrayList<StockItem> list = new ArrayList();
        String sql = "SELECT * FROM Stock_Item";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockItem item = StockItem.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public StockItem save(StockItem obj) {
        String sql = "INSERT INTO Stock_Item (Mat_Item_ID, Mat_Item_Price, Mat_Item_Unit, Mat_ID, Stock_Sheet_ID) "
                + "VALUES (?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMatItemID());
            stmt.setDouble(2, obj.getMatItemPrice());
            stmt.setString(3, obj.getMatItemUnit());
            stmt.setInt(4, obj.getMatID().getId());
            stmt.setInt(5, obj.getStockSheetID().getId());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertID(stmt);
            obj = get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public StockItem update(StockItem obj) {
        String sql = "UPDATE Stock_Item"
                + " SET Mat_Item_Price = ?, Mat_Item_Unit = ?, Mat_ID = ?, Stock_Sheet_ID = ?"
                + " WHERE Mat_Item_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMatItemID());
            stmt.setDouble(2, obj.getMatItemPrice());
            stmt.setString(3, obj.getMatItemUnit());
            stmt.setInt(4, obj.getMatID().getId());
            stmt.setInt(5, obj.getStockSheetID().getId());

            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(StockItem obj) {
        String sql = "DELETE FROM Stock_Item WHERE Mat_Item_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMatItemID());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    
}