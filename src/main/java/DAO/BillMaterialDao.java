/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Helper.DatabaseHelper;
import Model.BillMaterial;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author WIP
 */
public class BillMaterialDao implements Dao<BillMaterial> {

    @Override
    public BillMaterial get(int id) {
        BillMaterial item = null;
        String sql = "SELECT * FROM Bill_Material WHERE Bill_Mat_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = BillMaterial.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<BillMaterial> getAll() {
        ArrayList<BillMaterial> list = new ArrayList();
        String sql = "SELECT * FROM Bill_Material";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillMaterial item = BillMaterial.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public BillMaterial save(BillMaterial obj) {
        String sql = "INSERT INTO Bill_Material (Bill_Mat_ID, Bill_Mat_Name, Bill_Mat_Date, Mat_ID) "
                + "VALUES (?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setString(2, obj.getName());
            stmt.setDate(3, obj.getDate());
            stmt.setInt(4, obj.getMatID().getId());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertID(stmt);
            obj = get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public BillMaterial update(BillMaterial obj) {
        String sql = "UPDATE Bill_Material"
                + " SET Bill_Mat_Name = ?, Bill_Mat_Date = ?, Mat_ID = ?"
                + " WHERE Bill_Mat_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setString(2, sdf.format(obj.getDate()));
            stmt.setInt(3, obj.getMatID().getId());
            stmt.setInt(4, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(BillMaterial obj) {
        String sql = "DELETE FROM Bill_Material WHERE Bill_Mat_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
