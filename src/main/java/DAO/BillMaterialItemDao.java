/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Helper.DatabaseHelper;
import Model.BillMaterialItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author WIP
 */
public class BillMaterialItemDao implements Dao<BillMaterialItem> {

    @Override
    public BillMaterialItem get(int id) {
        BillMaterialItem item = null;
        String sql = "SELECT * FROM Bill_Material_Item WHERE Bill_Mat_Item_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = BillMaterialItem.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<BillMaterialItem> getAll() {
        ArrayList<BillMaterialItem> list = new ArrayList();
        String sql = "SELECT * FROM Bill_Material_Item";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillMaterialItem item = BillMaterialItem.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public BillMaterialItem save(BillMaterialItem obj) {
        String sql = "INSERT INTO Bill_Material_Item (Bill_Mat_Item_ID, Bill_Mat_Item_Price, Bill_Mat_Item_Unit, Bill_Mat_Item_QTY, Bill_Mat_Item_Discount, Bill_Mat_Item_Total, Bil_Mat_ID) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setDouble(2, obj.getPrice());
            stmt.setString(3, obj.getUnit());
            stmt.setInt(4, obj.getQty());
            stmt.setDouble(5, obj.getDiscount());
            stmt.setDouble(6, obj.getTotal());
            stmt.setInt(7, obj.getBillMatID().getId());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertID(stmt);
            obj = get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public BillMaterialItem update(BillMaterialItem obj) {
        String sql = "UPDATE Bill_Material_Item"
                + " SET Bill_Mat_Item_Price = ?, Bill_Mat_Item_Unit = ?, Bill_Mat_Item_QTY = ?, Bill_Mat_Item_Discount = ?, Bill_Mat_Item_Total = ?, Bil_Mat_ID = ?"
                + " WHERE Bill_Mat_Item_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getPrice());
            stmt.setString(2, obj.getUnit());
            stmt.setInt(3, obj.getQty());
            stmt.setDouble(4, obj.getDiscount());
            stmt.setDouble(5, obj.getTotal());
            stmt.setInt(6, obj.getBillMatID().getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(BillMaterialItem obj) {
        String sql = "DELETE FROM Bill_Material_Item WHERE Bill_Mat_Item_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
