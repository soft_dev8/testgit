/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Helper.DatabaseHelper;
import Model.ReceiptItem;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class ReceiptItemDao implements Dao<ReceiptItem>{

    @Override
    public ReceiptItem get(int id) {
        ReceiptItem item = null;
        String sql = "SELECT * FROM Receipt_Item WHERE Receipt_Item_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = ReceiptItem.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }
    
    public List<ReceiptItem> getAll() {
        ArrayList<ReceiptItem> list = new ArrayList();
        String sql = "SELECT * FROM Receipt_Item";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptItem item = ReceiptItem.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<ReceiptItem> getAll(String where, String order) {
        ArrayList<ReceiptItem> list = new ArrayList();
        String sql = "SELECT * FROM Receipt_Item where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptItem item = ReceiptItem.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<ReceiptItem> getByReceiptID(int receiptID) {
        return getAll("Receipt_ID ="+ receiptID," Receipt_Item_ID ASC ");
    }

    @Override
    public ReceiptItem save(ReceiptItem obj) {
        String sql = "INSERT INTO Receipt_Item (Receipt_Item_Name,Receipt_Item_QTY,Receipt_Item_Price,Receipt_ID,Prod_ID)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getQty());
            stmt.setDouble(3, obj.getPrice());
            stmt.setInt(4, obj.getReceipt().getId());
            stmt.setInt(5, obj.getProduct().getId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertID(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public ReceiptItem update(ReceiptItem obj) {
        String sql = "UPDATE Receipt_Item"
                + " SET Receipt_Item_Name = ?, Receipt_Item_QTY = ?, Receipt_Item_Price = ?, Receipt_ID = ?, Prod_ID = ?"
                + " WHERE Receipt_Item_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getQty());
            stmt.setDouble(3, obj.getPrice());
            stmt.setInt(4, obj.getReceipt().getId());
            stmt.setInt(5, obj.getProduct().getId());
            stmt.setInt(6, obj.getId());
            
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ReceiptItem obj) {
        String sql = "DELETE FROM Receipt_Item WHERE Receipt_Item_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1; 
    }
    
   
    
}
