/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Helper.DatabaseHelper;
import Model.Salary;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author WIP
 */
public class SalaryDao implements Dao<Salary> {

    @Override
    public Salary get(int id) {
        Salary item = null;
        String sql = "SELECT * FROM Salary WHERE Sal_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Salary.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<Salary> getAll() {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM Salary";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary item = Salary.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Salary save(Salary obj) {
        String sql = "INSERT INTO Salary (Sal_ID, Sal_Date, Sal_Total, Sal_Pay_Circle, Emp_ID, WIO_ID) "
                + "VALUES (?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setDate(2, obj.getDate());
            stmt.setDouble(3, obj.getTotal());
            stmt.setString(4, obj.getPayCircle());
            stmt.setInt(5, obj.getEmpID().getId());
            stmt.setInt(6, obj.getWioID().getId());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertID(stmt);
            obj = get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Salary update(Salary obj) {
       String sql = "UPDATE Salary"
                + " SET Sal_Date = ?, Sal_Total = ?, Sal_Pay_Circle = ?, Emp_ID = ?, WIO_ID = ?"
                + " WHERE Sal_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setDate(2, obj.getDate());
            stmt.setDouble(3, obj.getTotal());
            stmt.setString(4, obj.getPayCircle());
            stmt.setInt(5, obj.getEmpID().getId());
            stmt.setInt(6, obj.getWioID().getId());

            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Salary obj) {
        String sql = "DELETE FROM Salary WHERE Sal_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
