/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Helper.DatabaseHelper;
import Model.Receipt;
import Model.ReceiptItem;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class ReceiptDao implements Dao<Receipt> {

    @Override
    public Receipt get(int id) {
        ReceiptItemDao receiptItemDao = new ReceiptItemDao();
        Receipt item = null;
        String sql = "SELECT * FROM Receipt WHERE Receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Receipt.fromRS(rs);
                List<ReceiptItem> orderDetails = receiptItemDao.getByReceiptID(item.getId());
                item.setReceiptItem((ArrayList<ReceiptItem>) orderDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return item;
    }

    @Override
    public List<Receipt> getAll() {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM Receipt";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt item = Receipt.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<Receipt> getAll(String where, String order) throws ParseException {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM Receipt where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt item= Receipt.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Receipt> getAll(String order) throws ParseException {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM Receipt  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt item = Receipt.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Receipt save(Receipt obj) {
        ReceiptItemDao receiptItemDao = new ReceiptItemDao();
        String sql = "INSERT INTO Receipt (Receipt_Amount,Receipt_Date)"
                + "VALUES(?,?)";
        Connection conn = DatabaseHelper.getConnect();
        DatabaseHelper.beginTransaction();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getAmount());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stmt.setString(2, sdf.format(obj.getDate()));
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertID(stmt);
            //Add detail
            obj.setId(id);
            for(ReceiptItem od: obj.getReceiptItem()) {
                ReceiptItem detail = receiptItemDao.save(od);
                if(detail==null) {
                    DatabaseHelper.endTransactionWithRollback();
                    return null;
                }
            } 
            DatabaseHelper.endTransactionWithCommit();
            return get(id);
        } catch (SQLException ex) {
            DatabaseHelper.endTransactionWithRollback();
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Receipt update(Receipt obj) {
        String sql = "UPDATE Receipt"
                + " SET Receipt_Date = ?, Receipt_Amount = ?, Receipt_Discount = ?, Receipt_Receive = ?, Receipt_Change = ?"
                + ", Receipt_Payment_Type = ?"
                + " WHERE Receipt_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setString(1, sdf.format(obj.getDate()));
            stmt.setDouble(2, obj.getAmount());
            stmt.setDouble(3, obj.getDiscount());
            stmt.setDouble(4, obj.getReceive());
            stmt.setDouble(5, obj.getChange());
            stmt.setString(6, obj.getPaymentType());
            stmt.setInt(7, obj.getId());
            
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Receipt obj) {
        String sql = "DELETE FROM Receipt WHERE Receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
    
}
