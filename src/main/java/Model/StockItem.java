/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DAO.MaterialDao;
import DAO.StockSheetDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class StockItem {
    private int matItemID;
    private Double matItemPrice;
    private String matItemUnit;
    private Material matID;
    private StockSheet stockSheetID;

    public StockItem(int matItemID, Double matItemPrice, String matItemUnit, Material matID, StockSheet stockSheetID) {
        this.matItemID = matItemID;
        this.matItemPrice = matItemPrice;
        this.matItemUnit = matItemUnit;
        this.matID = matID;
        this.stockSheetID = stockSheetID;
    }

    public StockItem() {
        this.matItemID = -1 ;
    }

    public int getMatItemID() {
        return matItemID;
    }

    public void setMatItemID(int matItemID) {
        this.matItemID = matItemID;
    }

    public Double getMatItemPrice() {
        return matItemPrice;
    }

    public void setMatItemPrice(Double matItemPrice) {
        this.matItemPrice = matItemPrice;
    }

    public String getMatItemUnit() {
        return matItemUnit;
    }

    public void setMatItemUnit(String matItemUnit) {
        this.matItemUnit = matItemUnit;
    }

    public Material getMatID() {
        return matID;
    }

    public void setMatID(Material matID) {
        this.matID = matID;
    }

    public StockSheet getStockSheetID() {
        return stockSheetID;
    }

    public void setStockSheetID(StockSheet stockSheetID) {
        this.stockSheetID = stockSheetID;
    }

    @Override
    public String toString() {
        return "StockItem{" + "matItemID=" + matItemID + ", matItemPrice=" + matItemPrice + ", matItemUnit=" + matItemUnit + ", matID=" + matID + ", stockSheetID=" + stockSheetID + '}';
    }
    
    
   
    
     public static StockItem fromRS(ResultSet rs) {
        StockItem stockItem = new StockItem();
        MaterialDao material = new MaterialDao();
        StockSheetDao stockSheet = new StockSheetDao();
        try {
            stockItem.setMatItemID(rs.getInt("Mat_Item_ID"));
            stockItem.setMatItemPrice(rs.getDouble("Mat_Item_Price"));
            stockItem.setMatItemUnit(rs.getString("Mat_Item_Unit"));
            int matid = rs.getInt("Mat_ID");
            Material item = material.get(matid);
            stockItem.setMatID(item);
            
            
            int stockSheetid = rs.getInt("Stock_Sheet_ID");
            StockSheet item2 = stockSheet.get(stockSheetid);
            stockItem.setStockSheetID(item2);
            
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return stockItem;
    }
    
    
}
