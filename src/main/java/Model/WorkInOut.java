/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DAO.EmployeeDao;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class WorkInOut {

    private int id;
    private Date dateTimeIn;
    private Date dateTimeOut;
    private Employee empID;

    public WorkInOut(int id, Date dateTimeIn, Date dateTimeOut, Employee empID) {
        this.id = id;
        this.dateTimeIn = dateTimeIn;
        this.dateTimeOut = dateTimeOut;
        this.empID = empID;
    }

    public WorkInOut() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateTimeIn() {
        return dateTimeIn;
    }

    public void setDateTimeIn(Date dateTimeIn) {
        this.dateTimeIn = dateTimeIn;
    }

    public Date getDateTimeOut() {
        return dateTimeOut;
    }

    public void setDateTimeOut(Date dateTimeOut) {
        this.dateTimeOut = dateTimeOut;
    }

    public Employee getEmpID() {
        return empID;
    }

    public void setEmpID(Employee empID) {
        this.empID = empID;
    }

    @Override
    public String toString() {
        return "WorkInOut{" + "id=" + id + ", dateTimeIn=" + dateTimeIn + ", dateTimeOut=" + dateTimeOut + ", empID=" + empID + '}';
    }

    public static WorkInOut fromRS(ResultSet rs) {
        EmployeeDao employeeDao = new EmployeeDao();
        WorkInOut workInOut = new WorkInOut();
        try {
            workInOut.setId(rs.getInt("WIO_ID"));
            workInOut.setDateTimeIn(rs.getDate("Date_Time_In"));
            workInOut.setDateTimeOut(rs.getDate("Date_Time_Out"));
            int employeeId = rs.getInt("Emp_ID");
            Employee item = employeeDao.get(employeeId);
            workInOut.setEmpID(item);
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return workInOut;
    }

}
