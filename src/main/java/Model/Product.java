/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class Product {
    
    private int id;
    private String name;
    private Double price;
    private String type;

    public Product(int id, String name, Double price, String type) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.type = type;
    }

    public Product() {
        this.id = -1;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int Id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", type=" + type + '}';
    }
    
        public static Product fromRS(ResultSet rs) {
        Product product = new Product();
        try {
            product.setId(rs.getInt("Prod_ID"));
            product.setName(rs.getString("Prod_Name"));
            product.setPrice(rs.getDouble("Prod_Price"));
            product.setType(rs.getString("Prod_Type"));

        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }    
        
        
    public static ArrayList<Product> getMockProductList() {
        ArrayList<Product> list = new ArrayList<Product>();
        for (int i = 0; i < 10; i++) {
            list.add(new Product());
        }
        return list;
    }
    
}
