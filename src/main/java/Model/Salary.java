/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DAO.EmployeeDao;
import DAO.WorkInOutDao;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class Salary {

    private int id;
    private Date date;
    private double total;
    private String payCircle;
    private Employee empID;
    private WorkInOut wioID;

    public Salary(int id, Date date, int total, String payCircle, Employee empID, WorkInOut wioID) {
        this.id = id;
        this.date = date;
        this.total = total;
        this.payCircle = payCircle;
        this.empID = empID;
        this.wioID = wioID;
    }

    public Salary() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getPayCircle() {
        return payCircle;
    }

    public void setPayCircle(String payCircle) {
        this.payCircle = payCircle;
    }

    public Employee getEmpID() {
        return empID;
    }

    public void setEmpID(Employee empID) {
        this.empID = empID;
    }

    public WorkInOut getWioID() {
        return wioID;
    }

    public void setWioID(WorkInOut wioID) {
        this.wioID = wioID;
    }

    @Override
    public String toString() {
        return "Salary{" + "id=" + id + ", date=" + date + ", total=" + total + ", payCircle=" + payCircle + ", empID=" + empID + ", wioID=" + wioID + '}';
    }

    public static Salary fromRS(ResultSet rs) {
        EmployeeDao employeeDao = new EmployeeDao();
        WorkInOutDao workInOutDao = new WorkInOutDao();
        Salary salary = new Salary();
        try {
            salary.setId(rs.getInt("Sal_ID"));
            salary.setDate(rs.getDate("Sal_Date"));
            salary.setTotal(rs.getDouble("Sal_Total"));
            salary.setPayCircle(rs.getString("Sal_Pay_Circle"));
            int employeeId = rs.getInt("Emp_ID");
            Employee item = employeeDao.get(employeeId);
            salary.setEmpID(item);

            int wioID = rs.getInt("WIO_ID");
            WorkInOut item2 = workInOutDao.get(wioID);
            salary.setWioID(item2);

        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salary;
    }

}
