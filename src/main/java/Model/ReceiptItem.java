/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DAO.ProductDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class ReceiptItem {
    private int id;
    private Product product;
    private String name;
    private int qty;
    private int unit;
    private Double price;
    private Receipt receipt;

    public ReceiptItem(int id, Product product,String name, int qty, int unit ,Double price ,Receipt receipt) {
        this.id = id;
        this.product = product;
        this.name = name;
        this.qty = qty;
        this.unit = unit;
        this.price = price;
        this.receipt = receipt;
    }

    public ReceiptItem(Product product, String name, int qty, int unit, Double price, Receipt receipt) {
        this.product = product;
        this.name = name;
        this.qty = qty;
        this.unit = unit;
        this.price = price;
        this.receipt = receipt;
    }
    
    public ReceiptItem() {
        this.id = -1;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }
    
    

    @Override
    public String toString() {
        return "Reciept_Item{" + "id=" + id + ", name=" + name + ", qty=" + qty + ", unit=" + unit + ", price=" + price + '}';
    }
    
    public static ReceiptItem fromRS(ResultSet rs) {
        ProductDao  productDao = new ProductDao();
        ReceiptItem receiptItem = new ReceiptItem();
        try {
            receiptItem.setId(rs.getInt("Receipt_Item_ID"));
            receiptItem.setName(rs.getString("Receipt_Item_Name"));
            receiptItem.setQty(rs.getInt("Receipt_Item_QTY"));
            receiptItem.setPrice(rs.getDouble("Receipt_Item_"));
             int productId = rs.getInt("Prod_id");
            Product item = productDao.get(productId);
            receiptItem.setProduct(item);
        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receiptItem;
    }
    
}
