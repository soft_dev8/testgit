/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class Receipt {
    private int id;
    private Date date;
    private double amount;
    private double discount;
    private double receive;
    private double change;
    private String paymentType;
    private ArrayList<ReceiptItem> receiptItem;

    public Receipt(int id, Date date, double amount, double discount, double receive, double change, String paymentType, ArrayList<ReceiptItem> receiptItem) {
        this.id = id;
        this.date = date;
        this.amount = amount;
        this.discount = discount;
        this.receive = receive;
        this.change = change;
        this.paymentType = paymentType;
        this.receiptItem = receiptItem;
    }

    public Receipt() {
        this.id= -1;
        receiptItem = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getReceive() {
        return receive;
    }

    public void setReceive(double receive) {
        this.receive = receive;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
    
    public void setReceiptItem(ArrayList<ReceiptItem> receiptItem) {
        this.receiptItem = receiptItem;
    }

    public ArrayList<ReceiptItem> getReceiptItem() {
        return receiptItem;
    }

    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", date=" + date + ", amount=" + amount + ", discount=" + discount + ", receive=" + receive + ", change=" + change + ", paymentType=" + paymentType + ", receiptItem=" + receiptItem + '}';
    }
    
    public static Receipt fromRS(ResultSet rs) throws ParseException {
        Receipt receipt = new Receipt();
        try {
            receipt.setId(rs.getInt("Receipt_ID"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            receipt.setDate(sdf.parse(rs.getString("Receipt_Date")));
            receipt.setAmount(rs.getDouble("Receipt_Amount"));
            receipt.setDiscount(rs.getDouble("Receipt_Discount"));
            receipt.setReceive(rs.getDouble("Receipt_Receive"));
            receipt.setChange(rs.getDouble("Receipt_Change"));
            receipt.setPaymentType(rs.getString("Receipt_Payment_Type"));

        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receipt;
    }    
    
}
