/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author WIP
 */
public class Material {

    private int id;
    private String name;
    private int qty;
    private int request;

    public Material(int id, String name, int qty, int request) {
        this.id = id;
        this.name = name;
        this.qty = qty;
        this.request = request;
    }

    public Material() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getRequest() {
        return request;
    }

    public void setRequest(int request) {
        this.request = request;
    }

    @Override
    public String toString() {
        return "Material{" + "id=" + id + ", name=" + name + ", qty=" + qty + ", request=" + request + '}';
    }

    public static Material fromRS(ResultSet rs) {
        Material material = new Material();
        try {
            material.setId(rs.getInt("Mat_ID"));
            material.setName(rs.getString("Mat_Name"));
            material.setRequest(rs.getInt("Mat_Req"));
            material.setQty(rs.getInt("Mat_QTY"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return material;
    }

}
