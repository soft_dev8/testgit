/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DAO.ReceiptDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class Customer {

    private int id;
    private String name;
    private String phone;
    private String status;
    private int point;
    private Receipt receiptID;

    public Customer(int id, String name, String phone, String status, int point, Receipt receiptID) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.status = status;
        this.point = point;
        this.receiptID = receiptID;
    }

    public Customer(int id, String name, String phone, String status, int point) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.status = status;
        this.point = point;
    }

    public Customer() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public Receipt getReceiptID() {
        return receiptID;
    }

    public void setReceiptID(Receipt receiptID) {
        this.receiptID = receiptID;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", name=" + name + ", phone=" + phone + ", status=" + status + ", point=" + point + ", receiptID=" + receiptID + '}';
    }

    public static Customer fromRS(ResultSet rs) {
        Customer customer = new Customer();
        ReceiptDao reciept = new ReceiptDao();
        try {
            customer.setId(rs.getInt("Cus_ID"));
            customer.setName(rs.getString("Cus_Name"));
            customer.setPhone(rs.getString("Cus_Phone"));
            customer.setStatus(rs.getString("Cus_Status"));
            customer.setPoint(rs.getInt("Cus_Point"));

            int recID = rs.getInt("Receipt_ID");
            Receipt item = reciept.get(recID);
            customer.setReceiptID(item);

        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return customer;
    }

}
