/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class Employee {

    private int id;
    private String name;
    private String password;
    private String phone;
    private String type;
    private String position;

    public Employee(int id, String name, String password, String phone, String type, String position) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.phone = phone;
        this.type = type;
        this.position = position;
    }
    
    public Employee(String name , String password) {
        this.password = password;
        this.name = name;
    }

    public Employee() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int Id) {
        this.id = Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Employee{" + "Id=" + id + ", name=" + name + ", password=" + password + ", phone=" + phone + ", type=" + type + ", position=" + position + '}';
    }


    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("Emp_ID"));
            employee.setName(rs.getString("Emp_Name"));
            employee.setPassword(rs.getString("Emp_Password"));
            employee.setPhone(rs.getString("Emp_Phone"));
            employee.setType(rs.getString("Emp_Type"));
            employee.setPosition(rs.getString("Emp_Position"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }

}
