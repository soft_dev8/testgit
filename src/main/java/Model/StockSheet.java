/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DAO.EmployeeDao;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class StockSheet {
   private int id;
    private Date date;
     private Employee empID;

    public StockSheet(int id, Date date, Employee empID) {
        this.id = id;
        this.date = date;
        this.empID = empID;
    }

    public StockSheet() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Employee getEmpID() {
        return empID;
    }

    public void setEmpID(Employee empID) {
        this.empID = empID;
    }

    @Override
    public String toString() {
        return "StockSheet{" + "id=" + id + ", date=" + date + ", empID=" + empID + '}';
    }

    
  
    public static StockSheet fromRS(ResultSet rs) {
        StockSheet stockSheet = new StockSheet();
        EmployeeDao employee = new EmployeeDao();
        try {
            stockSheet.setId(rs.getInt("Stock_Sheet_ID"));
            stockSheet.setDate(rs.getDate("Stock_Sheet_Date"));
            
           
            int employeeId = rs.getInt("Emp_ID");
            Employee item = employee.get(employeeId);
            stockSheet.setEmpID(item);

            
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return stockSheet;
    }
    
}
