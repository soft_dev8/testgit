/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DAO.BillMaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 66955
 */
public class BillMaterialItem {

    private int id;
    private double price;
    private String unit;
    private int qty;
    private double discount;
    private double total;
    private BillMaterial billMatID;

    public BillMaterialItem(int id, double price, String unit, int qty, int discount, double total, BillMaterial billMatID) {
        this.id = id;
        this.price = price;
        this.unit = unit;
        this.qty = qty;
        this.discount = discount;
        this.total = total;
        this.billMatID = billMatID;
    }

    public BillMaterialItem() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public BillMaterial getBillMatID() {
        return billMatID;
    }

    public void setBillMatID(BillMaterial billMatID) {
        this.billMatID = billMatID;
    }

    @Override
    public String toString() {
        return "BillMaterialItem{" + "id=" + id + ", price=" + price + ", unit=" + unit + ", qty=" + qty + ", discount=" + discount + ", total=" + total + ", billMatID=" + billMatID + '}';
    }

    public static BillMaterialItem fromRS(ResultSet rs) {
        BillMaterialItem billMaterialitem = new BillMaterialItem();
        BillMaterialDao billmaterial = new BillMaterialDao();
        try {
            billMaterialitem.setId(rs.getInt("Bill_Mat_Item_ID"));
            billMaterialitem.setPrice(rs.getDouble("Bill_Mat_Item_Price"));
            billMaterialitem.setUnit(rs.getString("Bill_Mat_Item_Unit"));
            billMaterialitem.setQty(rs.getInt("Bill_Mat_Item_QTY"));
            billMaterialitem.setDiscount(rs.getDouble("Bill_Mat_Item_Discount"));
            billMaterialitem.setTotal(rs.getDouble("Bill_Mat_Item_Total"));

            int billMatid = rs.getInt("Bill_Mat_ID");
            BillMaterial item = billmaterial.get(billMatid);
            billMaterialitem.setBillMatID(item);

        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billMaterialitem;
    }

}
