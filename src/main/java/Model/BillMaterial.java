/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DAO.MaterialDao;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 66955
 */
public class BillMaterial {

    private int id;
    private String name;
    private Date date;
    private Material matID;

    public BillMaterial(int id, String name, Date date, Material matID) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.matID = matID;
    }

    public BillMaterial() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Material getMatID() {
        return matID;
    }

    public void setMatID(Material matID) {
        this.matID = matID;
    }

    @Override
    public String toString() {
        return "BillMaterial{" + "id=" + id + ", name=" + name + ", date=" + date + ", matID=" + matID + '}';
    }

    public static BillMaterial fromRS(ResultSet rs) {
        BillMaterial billMaterial = new BillMaterial();
        MaterialDao material = new MaterialDao();
        try {
            billMaterial.setId(rs.getInt("Bill_Mat_ID"));
            billMaterial.setName(rs.getString("Bill_Mat_Name"));
            billMaterial.setDate(rs.getDate("Bill_Mat_Date"));

            int matID = rs.getInt("Mat_ID");
            Material item = material.get(matID);
            billMaterial.setMatID(item);

        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billMaterial;
    }

}
