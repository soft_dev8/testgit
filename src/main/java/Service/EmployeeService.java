/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import DAO.EmployeeDao;
import Model.Employee;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 66955
 */
public class EmployeeService {
    
    public static Employee login(String name , String password) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.getByLogin(name);
        if(employee != null && employee.getPassword().equals(password)) {
            return employee;
        }
        return null;
    }
    
    public List<Employee>getEmployees() {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll(" Emp_name asc");
    }
    
}
